package com.epam.demo.service;

import com.epam.demo.consumer.wsdl.GetProductsInParams;
import com.epam.demo.consumer.wsdl.GetProductsOutResultSetRow;
import com.epam.demo.consumer.wsdl.GetProductsRequest;
import com.epam.demo.consumer.wsdl.GetProductsResponse;
import com.epam.demo.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarsService {
    Logger logger = LoggerFactory.getLogger(CarsService.class);

    private WebServiceTemplate webServiceTemplate;

    @Autowired
    public CarsService(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }

    public List<Product> getProducts(String brand) {
        GetProductsRequest request = new GetProductsRequest();
        GetProductsInParams inParams = new GetProductsInParams();
        inParams.setBrandName(brand);
        request.setInParams(inParams);
        GetProductsResponse response =
                (GetProductsResponse) webServiceTemplate
                        .marshalSendAndReceive(request);
        List<Product> cars = response.getOutParams().getResultSet().getResultSetRow().stream().map(this::mapRow).collect(Collectors.toList());
        for (Product product : cars) {
            logger.info(product.toString());
        }
        return cars;
    }

    public List<Product> getProductsByBrand(String brand) {
        GetProductsRequest request = new GetProductsRequest();
        GetProductsInParams inParams = new GetProductsInParams();
        inParams.setBrandName(brand);
        request.setInParams(inParams);
        GetProductsResponse response =
                (GetProductsResponse) webServiceTemplate
                        .marshalSendAndReceive(request);
        List<Product> cars = response.getOutParams().getResultSet().getResultSetRow().stream().map(this::mapRow).collect(Collectors.toList());
        for (Product product : cars) {
            logger.info(product.toString());
        }
        return cars;
    }

    private Product mapRow(GetProductsOutResultSetRow row) {
        Product car = new Product();
        car.setPrice(row.getPrice());
        car.setName(row.getName());
        car.setAmount(row.getAmount());
        car.setIdBrand(row.getIdBrand());
        car.setIdCategory(row.getIdCategory());
        car.setId(row.getIdProduct());
        return car;
    }

}

package com.epam.demo.controllers;

import com.epam.demo.domain.Product;
import com.epam.demo.service.CarsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class CarsController {

    private CarsService carsService;

    @Autowired
    public void setCarsService(CarsService carsService) {
        this.carsService = carsService;
    }

    @GetMapping("/main/prod")
    public List<Product> getCars() {
        return carsService.getProducts("123");
    }

    @GetMapping("/main/produ")
    public List<Product> getCarsByBrand(@RequestParam String brand) {
        return carsService.getProductsByBrand(brand);
    }
}
